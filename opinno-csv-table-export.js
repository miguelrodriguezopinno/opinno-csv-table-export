/**************************************************************
 *				jQuery Opinno Table Export
 *
 * @author Miguel Rodríguez <miguel.rodriguez@opinno.com>
 *************************************************************/


(function ( $ ) {

	$.fn.opinno_csv_table_export = function( options ){

		var settings = $.extend({
			where 		: "",		// The css selector where I'm going to attach the list
			row_offset	: 0,		// In which row I want to start the exporting
			col_offset 	: 0,		// If we want to skip a complete column
			download	: true,		// Should I generate a file or print the output?
			headers		: true,		// Should I include the table headers?
			file_title	: $(document).find("title").text() // The title of the generated file minus the extension
		} , options );

		var formats 	= [{0:"csv",1:"Export to CSV"}]; 	// Format list 
		var self 		= $(this);
		var out 		= "";

		if ( settings.where.length === 0 ) throw "A destination for the action buttons is needed";
		if ( !self.is("table") ) throw "Table export has to be called on a html table element";

		finder = "td";

		if ( settings.headers ) {
			settings.row_offset += 1;
			finder 				+= ",th";
		}


		






		/**
		 * Draw buttons
		 */
		draw = "<ul>";
		
		$(formats).each( function(index , value) {
			
			draw += "<li><a href='javascript:void(0);'>" + value[1] + "</a></li>";
		});

		draw += "</ul>";

		var links = $(settings.where).html(draw);





		



		/**
		 * On click action
		 */
		links.find("a").on("click", function(){
			
			var out = "";

			self.find("tr").each( function(tr_index, tr_value){

				if ( !settings.headers  &&  $(tr_value).find("th") ) return true;				
				if ( settings.row_offset == tr_index + 1 ) return true;

				
				$(tr_value).find( finder ).each( function( td_index , td_value ){

					if ( settings.col_offset > 0 && settings.col_offset == td_index + 1 ) return true;
					

					out += '"' + $(td_value).html()
								.replace(/(<([^>]+)>)/ig,"")
								.replace(/\n/g, '')
								.trim() + '",';

				});
				out = out.substr( 0 , out.length - 1 );
				out += "\n";
			});
			

			if ( settings.download ) {


				window.URL = window.webkitURL || window.URL;

				var contentType 		= 'text/csv';
				var csvFile 			= new Blob([out], {type: contentType});
				var a 					= document.createElement('a');
				a.download 				= settings.file_title + '.csv';
				a.href 					= window.URL.createObjectURL(csvFile);
				a.textContent 			= "download";
				a.dataset.downloadurl 	= [contentType, a.download, a.href].join(':');

				document.body.appendChild(a);
				a.click();
				a.remove();


			} else {

				var w = window.open();

			    $( w.document.body ).html(out);
			}

		});


		return this;

		
	};

} ( jQuery ));