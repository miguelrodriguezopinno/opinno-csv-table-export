# jQuery Opinno Table Export #

### Author: Miguel Rodríguez <miguel.rodriguez@opinno.com> ###


This is just a simple HTML table CSV exporter build as a jQuery extension. It'll work with any HTML table (altought it has been only tested in a Django project, it ***SHOULD*** work just fine as it's just a JS Script). 


## Options: ##

The plugin will accept some configuration parameters as follow : 

* **where** [Obligatory]: 
The css selector where I'm going to attach the list which contains the links to actually do the export. It must be a valid CSS selector. Default : "" [Empty string]


* **row_offset** [Optional]:
Sets in which row I want to start the exporting. It doesn't take into account the table headers. Default : 0


* **col_offset** [Optional]: 
Skips the firsts N columns. Default : 0


* **download** [Optional]: 
If true (the default value) it just prepares a CSV file and starts the download, else, it'll open a new tab (or window) and just prints the generated CSV string. Default : True


* **headers**	[Optional]:
If true, includes the table headers (the 'th' tags). Else, skips them. Default : True


* **file_title** [Optional]: 
The title of the generated file minus the extension. Default : The current's page title



## Usage : ##

Just include the JS file and then paste this code anywhere in your page :


```
#!javascript

jQuery("#table").opinno_csv_table_export({
	where       : ".opinno-csv-table-export",
	row_offset  : 1 ,
	col_offset  : 1 ,
	file_title  : "My neaty neat csv file"
});
```


And you're good to go!